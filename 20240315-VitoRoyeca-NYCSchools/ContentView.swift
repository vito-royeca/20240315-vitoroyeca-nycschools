//
//  ContentView.swift
//  20240315-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 3/15/24.
//

import SwiftUI
import BottomSheet

struct ContentView: View {
    @EnvironmentObject var viewModel: SchoolsViewModel
    @State var position: BottomSheetPosition = .relative(0.5)
    private let positions: [BottomSheetPosition] = [
        .relativeBottom(0.1),
        .relative(0.5),
        .relativeTop(1.0)
    ]
    
    var body: some View {
        bottomSheetView
    }

    private var bottomSheetView: some View {
        SchoolsMapView(position: $position)
            .bottomSheet(
                bottomSheetPosition: $position,
                switchablePositions: positions,
                title: viewModel.selectedSchool == nil ? "NYC High Schools" : nil,
//                title: nil,
                content: {
                    if viewModel.selectedSchool == nil {
                        NavigationView {
                            SchoolsListView(position: $position)
                        }
                    } else {
                        NavigationView {
                            SchoolDetailsView()
                        }
                    }
                }
            )
    }
}

#Preview {
    ContentView()
        .environmentObject(SchoolsViewModel())
        .environmentObject(SATResultsViewModel())
}
