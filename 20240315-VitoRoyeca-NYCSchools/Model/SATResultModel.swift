//
//  SATResultModel.swift
//  20240315-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 3/15/24.
//

import Foundation
import SwiftData

/// Maps the satResults JSON API to Swift struct
final class SATResultModel: Codable {
    // matches the API properties to Swift properties
    enum CodingKeys: String, CodingKey {
        case dbn
        case numOfSATTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
    
    var dbn: String
    var numOfSATTestTakers: Int
    var satCriticalReadingAvgScore: Int
    var satMathAvgScore: Int
    var satWritingAvgScore: Int
    
    init(dbn: String,
         numOfSATTestTakers: Int,
         satCriticalReadingAvgScore: Int,
         satMathAvgScore: Int,
         satWritingAvgScore: Int) {
        self.dbn = dbn
        self.numOfSATTestTakers = numOfSATTestTakers
        self.satCriticalReadingAvgScore = satCriticalReadingAvgScore
        self.satMathAvgScore = satMathAvgScore
        self.satWritingAvgScore = satWritingAvgScore
    }
    
    // properly maps JSON data types to Swift data types
    init(from decoder : Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
     
        self.dbn = try container.decode(String.self, forKey: .dbn)
        
        var string = try container.decode(String.self, forKey: .numOfSATTestTakers)
        self.numOfSATTestTakers = Int(string) ?? 0
        
        string = try container.decode(String.self, forKey: .satCriticalReadingAvgScore)
        self.satCriticalReadingAvgScore = Int(string) ?? 0
        
        string = try container.decode(String.self, forKey: .satMathAvgScore)
        self.satMathAvgScore = Int(string) ?? 0
        
        string = try container.decode(String.self, forKey: .satWritingAvgScore)
        self.satWritingAvgScore = Int(string) ?? 0
    }
}
