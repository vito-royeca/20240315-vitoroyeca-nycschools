//
//  SATResultObject+CoreDataProperties.swift
//  20240315-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 3/17/24.
//
//

import Foundation
import CoreData

/// SAT Result Core Data object
extension SATResultObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SATResultObject> {
        return NSFetchRequest<SATResultObject>(entityName: "SATResultObject")
    }

    @NSManaged public var dateCreated: Date?
    @NSManaged public var dateUpdated: Date?
    @NSManaged public var takers: Int32
    @NSManaged public var math: Int32
    @NSManaged public var reading: Int32
    @NSManaged public var writing: Int32
    
    // One-to-one mapping to school
    @NSManaged public var school: SchoolObject?

}

extension SATResultObject : Identifiable {

}
