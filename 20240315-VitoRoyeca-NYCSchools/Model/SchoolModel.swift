//
//  SchoolModel.swift
//  20240315-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 3/15/24.
//

import Foundation
import MapKit

/// Maps the school JSON API to Swift struct
final class SchoolModel: Codable {
    // matches the API properties to Swift properties
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case location
        case phoneNumber = "phone_number"
        case schoolEmail = "school_email"
        case website
        case latitude
        case longitude
    }

    var dbn: String
    var schoolName: String
    var overviewParagraph: String
    var location: String
    var phoneNumber: String
    var schoolEmail: String?
    var website: String
    var latitude: Double?
    var longitude: Double?
    
    init(dbn: String,
         schoolName: String,
         overviewParagraph: String,
         location: String,
         phoneNumber: String,
         schoolEmail: String?,
         website: String,
         latitude: Double?,
         longitude: Double?) {
        self.dbn = dbn
        self.schoolName = schoolName
        self.overviewParagraph = overviewParagraph
        self.location = location
        self.phoneNumber = phoneNumber
        self.schoolEmail = schoolEmail
        self.website = website
        self.latitude = latitude
        self.longitude = longitude
    }
    
    // properly maps JSON data types to Swift data types
    init(from decoder : Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.dbn = try container.decode(String.self, forKey: .dbn)
        self.schoolName = try container.decode(String.self, forKey: .schoolName)
        self.overviewParagraph = try container.decode(String.self, forKey: .overviewParagraph)
        self.location = try container.decode(String.self, forKey: .location)
        self.phoneNumber = try container.decode(String.self, forKey: .phoneNumber)

        if container.contains(.schoolEmail) {
            self.schoolEmail = try container.decode(String.self, forKey: .schoolEmail)
        }
        
        self.website = try container.decode(String.self, forKey: .website)
        
        if container.contains(.latitude) {
            let string = try container.decode(String.self, forKey: .latitude)
            self.latitude = Double(string)
        }

        if container.contains(.longitude) {
            let string = try container.decode(String.self, forKey: .longitude)
            self.longitude = Double(string)
        }
    }
}

