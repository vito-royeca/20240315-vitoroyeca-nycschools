//
//  SchoolObject+CoreDataClass.swift
//  20240315-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 3/17/24.
//
//

import Foundation
import CoreData
import MapKit

public class SchoolObject: NSManagedObject {
    // helper property to create a CLLocationCoordinate2D object
    lazy var coordinate: CLLocationCoordinate2D? = {
        if latitude == 0 && longitude == 0 {
            return nil
        }
        
        return CLLocationCoordinate2D(latitude: latitude,
                                      longitude: longitude)
    }()
}
