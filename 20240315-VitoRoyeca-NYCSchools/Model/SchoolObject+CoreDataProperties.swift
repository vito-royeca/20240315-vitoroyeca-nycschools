//
//  SchoolObject+CoreDataProperties.swift
//  20240315-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 3/17/24.
//
//

import Foundation
import CoreData

/// School Core Data object
extension SchoolObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SchoolObject> {
        return NSFetchRequest<SchoolObject>(entityName: "SchoolObject")
    }

    @NSManaged public var dateCreated: Date?
    @NSManaged public var dateUpdated: Date?
    @NSManaged public var dbn: String
    @NSManaged public var name: String
    @NSManaged public var overviewParagraph: String?
    @NSManaged public var location: String?
    @NSManaged public var phoneNumber: String?
    @NSManaged public var email: String?
    @NSManaged public var website: String?
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    
    // One-to-one mapping to SAT result
    @NSManaged public var satResult: SATResultObject?

}

extension SchoolObject : Identifiable {

}
