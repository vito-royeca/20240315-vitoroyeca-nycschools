//
//  APIManager.swift
//  20240315-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 3/17/24.
//

import Foundation

/// Different types of error encountered
enum APIManagerError: Error {
    case invalidURL
    case httpError
    case unknownError
}

class APIManager {
    /// The API endpoint to get the schools records
    let schoolsEndpoint    = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    
    /// The API endpoint to get the SAT Results. We pass a `dbn` parameter to get only 1 record
    let satResultsEndpoint = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    
    /// Singleton property
    static let shared = APIManager()
    private init() {
        
    }

    /**
        func fetchSchools() async throws
        
        This methods fetches the school records from the API and then saves the records to Core Data
     */
    func fetchSchools() async throws {
        do {
            // check if we need to call the API. If we found an existing record
            // it is less than `CoreDataManager.cacheAge` (60 mins), then will not
            // call the API. We will read from the existing records in Core Data
            if try !CoreDataManager.shared.willFetchSchools() {
                return
            }

            // check if we have a valid URL
            guard let url = URL(string: schoolsEndpoint) else {
                throw APIManagerError.invalidURL
            }

            // Call the API and return a tuple of data and response
            let (data,response) = try await URLSession.shared.data(from: url)
            
            // check that the API response is OK: HTTP 200
            guard let response = response as? HTTPURLResponse,
                  response.statusCode == 200 else {
                throw APIManagerError.httpError
            }
            
            // parse the data returned data. The returned data is an array of `SchoolModel` struct
            let results = try JSONDecoder().decode([SchoolModel].self,
                                                  from: data)
            
            // save each result to Core Data
            for result in results {
                try CoreDataManager.shared.save(school: result)
            }
        } catch {
            throw APIManagerError.unknownError
        }
    }
    
    /**
        fetchSATResults(dbn: String) async throws
        
        This methods fetches the SAT Result records from the API and then saves the record to Core Data
        - parameter dbn: The school's dbn
     */
    func fetchSATResults(dbn: String) async throws {
        do {
            // check if we need to call the API. If we found an existing record
            // it is less than `CoreDataManager.cacheAge` (60 mins), then will not
            // call the API. We will read from the existing records in Core Data
            if try !CoreDataManager.shared.willFetchSATResult(dbn: dbn) {
                return
            }

            // check if we have a valid URL. We pass the `dbn` parameter to get only
            // that school's SAT Result
            guard let url = URL(string: "\(satResultsEndpoint)?dbn=\(dbn)") else {
                throw APIManagerError.httpError
            }

            // Call the API and return a tuple of data and response
            let (data,response) = try await URLSession.shared.data(from: url)
            
            // check that the API response is OK: HTTP 200
            guard let response = response as? HTTPURLResponse,
                  response.statusCode == 200 else {
                throw APIManagerError.httpError
            }
            
            // parse the data returned data. The returned data is an array of `SATResultModel` struct.
            // we only get 1 record because we pass the `dbn` parameter
            let results = try JSONDecoder().decode([SATResultModel].self,
                                                  from: data)
            
            // save each result to Core Data
            for result in results {
                try CoreDataManager.shared.save(satResult: result)
            }
        } catch {
            throw APIManagerError.unknownError
        }
    }
}
