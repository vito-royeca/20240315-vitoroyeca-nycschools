//
//  CoreDataManager.swift
//  20240315-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 3/17/24.
//

import Foundation
import CoreData

enum CoreDataManagerError: Error {
    case saveError
    case findError
}

class CoreDataManager: NSObject, ObservableObject {
    /// Singleton property
    static let shared = CoreDataManager()
    
    /// If we call the API, we must first check if we have existing data in Core Data that is
    /// less than or equal to `cacheAge`
    let cacheAge = 60 // 60 mins

    /// Context for viewing/fetching data from Core Data
    fileprivate var viewContext: NSManagedObjectContext
    
    /// Context for updating data in Core Data
    fileprivate var privateContext: NSManagedObjectContext
    
    /// Private initializer for singleton.
    private override init() {
        // our Core Data persistence store
        let persistentStore = PersistentStore(inMemory: false)
        
        // assign our private contexts
        self.viewContext = persistentStore.viewContext
        self.privateContext = persistentStore.privateContext
        
        super.init()
    }
    
    /// Save Core Data changes if there are any.
    func save() throws {
        if privateContext.hasChanges {
            do {
                try privateContext.save()
            } catch {
                throw CoreDataManagerError.saveError
            }
        }
    }

    /// func save(school: SchoolModel) throws
    /// - parameter school: The school JSON object model
    func save(school: SchoolModel) throws {
        do {
            // let us find an existing school object first, else we create
            // a new one if there is none
            let schoolObject = try findSchool(dbn: school.dbn, in: privateContext) ??
                SchoolObject(context: privateContext)
            
            // assign the properties from JSON to Core Data
            schoolObject.dbn = school.dbn
            schoolObject.name = school.schoolName
            schoolObject.overviewParagraph = school.overviewParagraph
            
            // let us remove the (lat, long) from the location property.
            if let endIndex = school.location.lastIndex(of: "(") {
                let locationString = school.location[school.location.startIndex ..< endIndex]
                schoolObject.location = String(locationString)
            }

            schoolObject.phoneNumber = school.phoneNumber
            schoolObject.email = school.schoolEmail
            schoolObject.website = school.website
            
            // some schools do not have latitude and longitude properties
            // but they are stored in the location property. here we check if
            // location has (lat, long) part and assign it to latitude and longitude
            //
            // NOTE: Some schools do not have (lat, long) part in their location, and also do not
            // have latitude and longitude values. In that case, we filter them out and do not show in the
            // map. See ``SchoolsMapView.annotationItems``. If given enough time, we should call Apple's
            // Map API to get the latitude and longitude from the location string.
            let lat = school.latitude ?? 0
            let lng = school.longitude ?? 0
            
            if lat == 0 || lng == 0,
               let startIndex = school.location.lastIndex(of: "("),
               let endIndex = school.location.lastIndex(of: ")") {
                
                let newStartIndex = school.location.index(startIndex, offsetBy: 1)
                let locationString = school.location[newStartIndex ..< endIndex]
                let components = locationString.components(separatedBy: ", ")
                
                if let first = components.first,
                   let last = components.last,
                   let newLat = Double(first),
                   let newLng = Double(last) {
                    if newLat != 0 && newLng != 0 {
                        schoolObject.latitude = newLat
                        schoolObject.longitude = newLng
                    }
                }
            } else {
                schoolObject.latitude = lat
                schoolObject.longitude = lng
            }
        
            // assign dateCreated if schoolObject does not have one,
            // i.e. it's newly fetched from the API
            if schoolObject.dateCreated == nil {
                schoolObject.dateCreated = Date()
            }
            
            // update the dateUpdated property everytime
            schoolObject.dateUpdated = Date()
        
            // save to Core Data
            try save()
        } catch {
            throw CoreDataManagerError.saveError
        }
    }
    
    /// func save(satResult: SATResultModel) throws
    /// - parameter satResult: The satResult JSON object model
    func save(satResult: SATResultModel) throws {
        do {
            // let us find an existing school object first, else we create
            // a new one if there is none
            let satResultObject = try findSATResult(dbn: satResult.dbn, in: privateContext) ??
                SATResultObject(context: privateContext)
            
            // assign the properties from JSON to Core Data
            satResultObject.takers = Int32(satResult.numOfSATTestTakers)
            satResultObject.reading = Int32(satResult.satCriticalReadingAvgScore)
            satResultObject.math = Int32(satResult.satMathAvgScore)
            satResultObject.writing = Int32(satResult.satWritingAvgScore)
            
            // assign the satResult to school
            satResultObject.school = try findSchool(dbn: satResult.dbn, in: privateContext)
        
            // assign dateCreated if satResultObject does not have one,
            // i.e. it's newly fetched from the API
            if satResultObject.dateCreated == nil {
                satResultObject.dateCreated = Date()
            }
            
            // update the dateUpdated property everytime
            satResultObject.dateUpdated = Date()
            
            // save to Core Data
            try save()
        } catch {
            throw CoreDataManagerError.saveError
        }
    }

    /// func willFetchSchools() throws -> Bool
    /// returns: Bool if we will call the API
    ///
    /// We must check if we have existing data in Core Data that is
    /// less than or equal to ``cacheAge``, otherwise return true (meaning we need to call the API)
    func willFetchSchools() throws -> Bool {
        // fetch the latest schoolObject
        let request = SchoolObject.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "dateUpdated", ascending: false)]
        request.fetchLimit = 1

        do {
            // check against `cacheAge`
            let result = try viewContext.fetch(request)
            if let first = result.first,
               let lastUpdated = first.dateUpdated,
               let diff = Calendar.current.dateComponents([.minute],
                                                          from: lastUpdated,
                                                          to: Date()).minute {
                return diff >= cacheAge
            } else {
                return true
            }
            
        } catch {
            throw CoreDataManagerError.findError
        }
    }

    /// func willFetchSATResult(dbn: String) throws -> Bool
    /// parameter dbn: String the school's dbn
    /// returns: Bool if we will call the API
    /// Note: we must check if we have existing data in Core Data that is
    /// less than or equal to ``cacheAge``, otherwise return true (meaning we need to call the API)
    func willFetchSATResult(dbn: String) throws -> Bool {
        // fetch the latest satResultObject
        let request = SATResultObject.fetchRequest()
        request.predicate = NSPredicate(format: "school.dbn = %@", dbn)
        request.sortDescriptors = [NSSortDescriptor(key: "dateUpdated", ascending: false)]
        request.fetchLimit = 1

        do {
            // check against `cacheAge`
            let result = try viewContext.fetch(request)
            if let first = result.first,
               let lastUpdated = first.dateUpdated,
               let diff = Calendar.current.dateComponents([.minute],
                                                          from: lastUpdated,
                                                          to: Date()).minute {
                return diff >= cacheAge
            } else {
                return true
            }
            
        } catch {
            throw CoreDataManagerError.findError
        }
    }

    /// func findAllSchools() throws -> [SchoolObject]
    /// return: [SchoolObject]
    ///
    /// Fetches all schools object from Core Data, sorted by name in ascending order
    func findAllSchools() throws -> [SchoolObject] {
        let request = SchoolObject.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        
        do {
            let result = try viewContext.fetch(request)
            return result
        } catch {
            throw CoreDataManagerError.findError
        }
    }

    /// func findSchools(query: String) throws -> [SchoolObject]
    /// return: [SchoolObject]
    ///
    /// Fetches all schools object from Core Data, sorted by name in ascending order, filtered in `name` by `query`
    /// If the query has 1 length, we return all school objects that begin with that query.
    /// If the query has 0 length, we return all school objects.
    /// Otherwise, we return all school objects whose name contains ``query``.
    func findSchools(query: String) throws -> [SchoolObject] {
        let request = SchoolObject.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]

        if query.count == 1 {
            request.predicate = NSPredicate(format: "name BEGINSWITH[cd] %@", query)
        } else if query.count == 0 {
            request.predicate = nil
        } else {
            request.predicate = NSPredicate(format: "name CONTAINS[cd] %@", query)
        }
        
        do {
            let result = try viewContext.fetch(request)
            return result
        } catch {
            throw CoreDataManagerError.findError
        }
    }

    /// findSchool(dbn: String, in context: NSManagedObjectContext? = nil) throws -> SchoolObject?
    /// parameter dbn: the school's dbn
    /// parameter context: the context to fetch from
    /// return: SchoolObject?
    /// Fetches a schoolObject filtered via ``dbn``
    func findSchool(dbn: String, in context: NSManagedObjectContext? = nil) throws -> SchoolObject? {
        let request = SchoolObject.fetchRequest()
        request.predicate = NSPredicate(format: "dbn = %@", dbn)
        
        do {
            let result = context == nil ? try viewContext.fetch(request) : try context?.fetch(request)
            return result?.first
        } catch {
            throw CoreDataManagerError.findError
        }
    }
    
    /// func findSATResult(dbn: String, in context: NSManagedObjectContext? = nil) throws -> SATResultObject?
    /// parameter dbn: the school's dbn
    /// parameter context: the context to fetch from
    /// return: SchoolObject?
    /// Fetches a satResultObject filtered via ``dbn``
    func findSATResult(dbn: String, in context: NSManagedObjectContext? = nil) throws -> SATResultObject? {
        let request = SATResultObject.fetchRequest()
        request.predicate = NSPredicate(format: "school.dbn = %@", dbn)
        
        do {
            let result = context == nil ? try viewContext.fetch(request) : try context?.fetch(request)
            return result?.first
        } catch {
            throw CoreDataManagerError.findError
        }
    }
}
