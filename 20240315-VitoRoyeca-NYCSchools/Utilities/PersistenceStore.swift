//
//  PersistenceStore.swift
//  20240315-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 3/17/24.
//

import CoreData

/// Core Data Persistence store
struct PersistentStore {
    /// persistence container
    let container: NSPersistentContainer

    /// init(inMemory: Bool = false)
    /// parameter inMemory: Bool configures the container for read-only or disk-based persistence
    init(inMemory: Bool = false) {
        container = NSPersistentContainer(name: "Schools")
        
        if inMemory {
            container.persistentStoreDescriptions.first!.url = URL(fileURLWithPath: "/dev/null")
        }

        // automatically saves changes from privateContext
        container.viewContext.automaticallyMergesChangesFromParent = true
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
    }
    
    /// context for fetching and viewwing data
    var viewContext: NSManagedObjectContext {
        container.viewContext
    }
    
    /// context for saving and updating data
    var privateContext: NSManagedObjectContext {
        container.newBackgroundContext()
    }
}
