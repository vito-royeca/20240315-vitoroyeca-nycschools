//
//  ErrorView.swift
//  20240315-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 3/16/24.
//

import SwiftUI

/// Shows an error View
struct ErrorView: View {
    /// the action to be performed when the "Try again" button is tapped
    var retryAction: () -> Void

    var body: some View {
        VStack(spacing: 20) {
            Label("Something went wrong.",
                  systemImage: "exclamationmark.circle")
            .foregroundColor(.red)
            
            Button("Try again") {
                retryAction()
            }
        }
    }
}

#Preview {
    ErrorView(retryAction: {
        print("Retrying...")
    })
}
