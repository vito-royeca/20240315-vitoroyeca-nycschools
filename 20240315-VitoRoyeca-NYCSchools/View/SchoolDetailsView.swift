//
//  SchoolDetailsView.swift
//  20240315-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 3/16/24.
//

import SwiftUI

/// View for displaying school details
struct SchoolDetailsView: View {
    // environment objects
    @EnvironmentObject var viewModel: SchoolsViewModel
    @EnvironmentObject var satViewModel: SATResultsViewModel
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        Group {
            // show different states
            if viewModel.isBusy {
                ProgressView()
            } else if viewModel.isFailed {
                ErrorView {
                    fetchSATResults()
                }
            } else {
                infoView
            }
        }
        // we only show ``satResultsView`` if we have valid data
        .navigationTitle(satViewModel.isSATResultsAvailable() ? "Average SAT Scores" : "No SAT Results Available")
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarBackButtonHidden(true)
        .toolbar {
            ToolbarItem(placement: .topBarTrailing) {
                Button(action: {
                    withAnimation {
                        viewModel.highlight(school: nil)
                    }
                    dismiss()
                }) {
                    Image(systemName: "x.circle")
                }
            }
        }
        .onAppear {
            withAnimation {
                // centers the map to the school's coordinates
                viewModel.highlight(school: viewModel.selectedSchool)
            }
            fetchSATResults()
        }
    }
    
    // contains all the views
    var infoView: some View {
        List {
            // we only show ``satResultsView`` if we have valid data
            if satViewModel.isSATResultsAvailable() {
                satResultsView
                    .listRowSeparator(.hidden)
            }
            
            Text(viewModel.selectedSchool?.name ?? "")
                .font(.system(size: 20))
                .fontWeight(.bold)
                .listRowSeparator(.hidden)

            contactView
                .listRowSeparator(.hidden)
            
            Text(viewModel.selectedSchool?.overviewParagraph ?? "")
                .listRowSeparator(.hidden)
        }
        .listStyle(.plain)
    }
    
    // shows the SAT Results data in blue text
    var satResultsView: some View {
        HStack {
            Spacer()
            VStack {
                Text("\(satViewModel.satResult?.math ?? 0)")
                    .font(.system(size: 30))
                    .foregroundColor(.blue)
                    .fontWeight(.bold)
                
                Text("Math")
                    .font(.system(size: 13))
                    .foregroundColor(.blue)
            }
            Spacer()
            VStack {
                Text("\(satViewModel.satResult?.reading ?? 0)")
                    .font(.system(size: 30))
                    .foregroundColor(.blue)
                    .fontWeight(.bold)
                
                Text("Reading")
                    .font(.system(size: 13))
                    .foregroundColor(.blue)
            }
            Spacer()
            VStack {
                Text("\(satViewModel.satResult?.writing ?? 0)")
                    .font(.system(size: 30))
                    .foregroundColor(.blue)
                    .fontWeight(.bold)
                
                Text("Writing")
                    .font(.system(size: 13))
                    .foregroundColor(.blue)
            }
            Spacer()
        }
    }
    
    // shows the contact information in gray text
    var contactView: some View {
        VStack(alignment: .leading) {
            if let locationString = viewModel.selectedSchool?.location {
                Label(locationString,
                      systemImage: "house")
                    .font(.subheadline)
                    .foregroundStyle(.gray)
            }
            
            if let phoneNumber = viewModel.selectedSchool?.phoneNumber {
                Label(phoneNumber,
                      systemImage: "phone")
                    .font(.subheadline)
                    .foregroundStyle(.gray)
            }
            
            if let schoolEmail = viewModel.selectedSchool?.email {
                Label(schoolEmail,
                      systemImage: "mail")
                    .font(.subheadline)
                    .foregroundStyle(.gray)
            }

            if let website = viewModel.selectedSchool?.website {
                Label(website,
                      systemImage: "safari")
                    .font(.subheadline)
                    .foregroundStyle(.gray)
            }
        }
    }

    /// func fetchSATResults()
    ///
    /// Fetches rhe SAT REsults data for this school
    func fetchSATResults() {
        Task {
            if let school = viewModel.selectedSchool {
                await satViewModel.fetchSATResults(dbn: school.dbn)
            }
        }
    }
}

#Preview {
    SchoolDetailsView()
        .environmentObject(SchoolsViewModel())
        .environmentObject(SATResultsViewModel())
}
