//
//  SchoolsListView.swift
//  20240315-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 3/15/24.
//

import SwiftUI
import BottomSheet

// Shows the list of searchable schools
struct SchoolsListView: View {
    // environment objects
    @EnvironmentObject var viewModel: SchoolsViewModel
    
    // Tracks the bottomSheetPosition.
    @Binding var position: BottomSheetPosition

    var body: some View {
        // iOS 17.0 changed the .onChange(:) method signature
        if #available(iOS 17.0, *) {
            navigationView
                .onChange(of: viewModel.searchText) {
                    filterSchools()
                }
        } else {
            navigationView
                .onChange(of: viewModel.searchText) { _ in
                    filterSchools()
                }
        }
    }
    
    var navigationView: some View {
        Group {
            // show different states
            if viewModel.isBusy {
                ProgressView()
            } else if viewModel.isFailed {
                ErrorView {
                    fetchSchools()
                }
            } else {
                listView
            }
        }
        .navigationBarTitleDisplayMode(.inline)
        .searchable(text: $viewModel.searchText,
                    prompt: "Search")
        .onAppear {
            fetchSchools()
        }
        .onSubmit(of: .search) {
            // handles the user typing on the search field
            filterSchools()
        }
    }

    // the list view of schools
    var listView: some View {
        List {
            ForEach(viewModel.schools,
                    id:\.dbn) { school in
                VStack(alignment: .leading) {
                    Text(school.name)
                    Text(school.location ?? "")
                        .font(.subheadline)
                        .foregroundStyle(.gray)
                }
                .onTapGesture {
                    viewModel.selectedSchool = school
                    position = .relative(0.5)
                }
            }
        }
        .listStyle(.plain)
    }
    
    /// private func fetchSchools()
    ///
    /// Fetches the schools data
    private func fetchSchools() {
        Task {
            await viewModel.fetchSchools()
        }
    }
    
    /// private func filterSchools()
    ///
    /// Filters the school data
    private func filterSchools() {
        viewModel.filterSchools()
    }
}

#Preview {
    SchoolsListView(position: .constant(.relative(0.5)))
        .environmentObject(SchoolsViewModel())
        .environmentObject(SATResultsViewModel())
}
