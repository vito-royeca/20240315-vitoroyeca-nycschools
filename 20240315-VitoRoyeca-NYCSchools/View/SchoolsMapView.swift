//
//  SchoolsMapView.swift
//  20240315-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 3/15/24.
//

import SwiftUI
import MapKit
import BottomSheet

/// Shows the map of NYC
struct SchoolsMapView: View {
    static let defaultRegion = MKCoordinateRegion(
        // NYC coordinates
        center: CLLocationCoordinate2D(latitude: 40.73,
                                       longitude: -74),
        span: MKCoordinateSpan(latitudeDelta: 1,
                               longitudeDelta: 1)
    )
    
    // environment object
    @EnvironmentObject var viewModel: SchoolsViewModel
    
    // Tracks the bottomSheetPosition.
    @Binding var position: BottomSheetPosition
    
    // Tracks the "Show All" toggle
    @State private var showAll = false
    
    // Tracks the selected school via the ``dbn`` property
    @State private var selectedSchool: String?

    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            mapView()
            
            // we only show the ``mapViewToggle`` if we have not selected a school
            if viewModel.selectedSchool == nil {
                mapToggleView
            }
        }
    }
    
    @ViewBuilder
    func mapView() -> some View {
        // iOS 17.0 changed the Map methods
        if #available(iOS 17.0, *) {
            let binding = Binding(
                get: { MapCameraPosition.region(viewModel.mapRegion) },
                set: { viewModel.mapRegion = $0.region! }
            )
            Map(position: binding,
                selection: $selectedSchool) {
                if let school = viewModel.selectedSchool {
                    if let coordinate = school.coordinate {
                        // show the selected school's map pin
                        Marker(school.name,
                               systemImage: "mappin",
                               coordinate: coordinate)
                    }
                } else {
                    if showAll {
                        // show all school map pins
                        ForEach(viewModel.schools) { school in
                            if let coordinate = school.coordinate {
                                Marker(school.name,
                                       systemImage: "mappin",
                                       coordinate: coordinate)
                                    .tag(school.dbn)
                                    .annotationTitles(.hidden)
                            }
                        }
                    }
                }
            }
            .edgesIgnoringSafeArea(.all)
            .onChange(of: viewModel.schools) {
                viewModel.mapRegion = MapCameraPosition.automatic.region ?? SchoolsMapView.defaultRegion
            }
            .onChange(of: selectedSchool) {
                // zoom in to the selected school and show its details
                if let school = viewModel.schools.filter({ $0.dbn == selectedSchool }).first {
                    viewModel.selectedSchool = school
                    position = .relative(0.5)
                }
            }

        } else {
            Map(coordinateRegion: $viewModel.mapRegion,
                annotationItems: annotationItems()) { item in
                // show all school map pins, or show the selected school's map pin
                MapAnnotation(coordinate: item.coordinate ?? CLLocationCoordinate2D()) {
                    Image(systemName: "mappin.circle.fill")
                        .foregroundColor(.red)
                        .onTapGesture {
                            selectedSchool = item.dbn
                        }
                }
            }
            .edgesIgnoringSafeArea(.all)
            .onChange(of: selectedSchool) { value in
                // zoom in to the selected school and show its details
                if let school = viewModel.schools.filter({ $0.dbn == value }).first {
                    viewModel.selectedSchool = school
                    position = .relative(0.5)
                }
            }
        }
    }
    
    // A toggle view located in upper right to show/hide all school map pins
    var mapToggleView: some View {
        GeometryReader { reader in
            VStack  {
                Toggle("Show All",
                       systemImage: showAll ? "eye" : "eye.slash",
                       isOn: $showAll)
                .padding(EdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10))
            }
            .background(.ultraThinMaterial)
            .cornerRadius(10)
            .padding(.leading, reader.size.width / 2)
            .padding(.trailing, 10)
        }
    }
    
    /// func annotationItems() -> [SchoolObject]
    /// returns [SchoolObject]: the school objects for showing pins on the map
    ///
    /// Some schools do not have (lat, long) part in their location, and also do not
    /// have latitude and longitude values. In that case, we filter them out and do not show in the
    /// map. If given enough time, we should call Apple's Map API to get the latitude and longitude from the location string.
    func annotationItems() -> [SchoolObject] {
        var items = [SchoolObject]()
        
        if let school = viewModel.selectedSchool {
            // show only the selected school
            items.append(school)
        } else {
            if showAll {
                // show all the schools
                items.append(contentsOf: viewModel.schools)
            }
        }
        
        // return only the schools with valid coordinates
        return items.filter { $0.coordinate != nil }
    }
}

#Preview {
    SchoolsMapView(position: .constant(.relative(0.5)))
        .environmentObject(SchoolsViewModel())
}

