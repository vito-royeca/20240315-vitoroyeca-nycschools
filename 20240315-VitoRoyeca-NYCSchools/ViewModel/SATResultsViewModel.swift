//
//  SATResultsViewModel.swift
//  20240315-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 3/15/24.
//

import SwiftUI

/// Handles SATResults published variables for the SwiftUI views
class SATResultsViewModel: ObservableObject {
    /// the satResults object
    @Published var satResult: SATResultObject?
    
    /// Tracks if we are calling the API and other heavy tasks
    @Published var isBusy = false
    
    /// Tracks if we encounter an error
    @Published var isFailed = false

    /// func fetchSATResults(dbn: String) async
    /// parameter dbn: the school's dbn
    ///
    /// Fetches the school's SAT Result object from the API and then saves it to Core Data.
    /// UI updates are called in the MainThread
    func fetchSATResults(dbn: String) async {
        if  isBusy {
            return
        }

        DispatchQueue.main.async {
            self.isBusy = true
            self.isFailed = false
        }

        do {
            // calls the API
            try await APIManager.shared.fetchSATResults(dbn: dbn)
            
            // saves to core data
            let result = try CoreDataManager.shared.findSATResult(dbn: dbn)

            DispatchQueue.main.async {
                self.isBusy = false
                self.isFailed = false
                
                // publishes the Core Data object to SwiftUI views
                self.satResult = result
            }
        } catch {
            DispatchQueue.main.async {
                self.isBusy = false
                self.isFailed = true
            }
        }
    }
    
    /// func isSATResultsAvailable() -> Bool
    ///
    /// Checks if we have a satResult values for this schoolObject
    func isSATResultsAvailable() -> Bool {
        satResult?.math != nil &&
        satResult?.reading != nil &&
        satResult?.writing != nil
    }
}
