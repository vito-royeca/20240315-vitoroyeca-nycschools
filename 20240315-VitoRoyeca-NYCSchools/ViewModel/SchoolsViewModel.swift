//
//  SchoolsViewModel.swift
//  20240315-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 3/15/24.
//

import Foundation
import SwiftUI
import MapKit

/// Handles Schools published variables for the SwiftUI views
class SchoolsViewModel: ObservableObject, Observable {
    /// the schools object
    @Published var schools = [SchoolObject]()
    
    /// the selected school from ``SchoolsMapView`` or ``SchoolsListView``
    @Published var selectedSchool: SchoolObject?
    
    /// the search text from the ``SchoolsListView`` search filter
    @Published var searchText = ""
    
    /// The ``SchoolsMapView`` region. Transitions to the ``selectedSchool`` 's coordinates,
    /// otherwise transitions back to ``SchoolsMapView.defaultRegion``
    @Published var mapRegion = SchoolsMapView.defaultRegion
    
    /// Tracks if we are calling the API and other heavy tasks
    @Published var isBusy = false
    
    /// Tracks if we encounter an error
    @Published var isFailed = false
    
    /// func fetchSchools() async
    ///
    /// Fetches schools object from the API and then saves them to Core Data.
    /// UI updates are called in the MainThread
    func fetchSchools() async {
        if !schools.isEmpty || isBusy {
            return
        }

        DispatchQueue.main.async {
            self.isBusy = true
            self.isFailed = false
        }

        do {
            // calls the API
            try await APIManager.shared.fetchSchools()
            
            // saves to core data
            let schools = try CoreDataManager.shared.findAllSchools()

            DispatchQueue.main.async {
                self.isBusy = false
                self.isFailed = false
                
                // publishes Core Data objects to SwiftUI views
                self.schools = schools
            }
        } catch {
            DispatchQueue.main.async {
                self.isBusy = false
                self.isFailed = true
            }
        }
    }

    /// func filterSchools()
    ///
    /// Fetches schools from Core Data filtered by name via ``searchText``
    func filterSchools() {
        isFailed = false
        
        do {
            schools = try CoreDataManager.shared.findSchools(query: searchText)
        } catch {
            isFailed = true
        }
    }
    
    /// func highlight(school: SchoolObject?)
    /// parameter school: SchoolObject? the selected school
    ///
    /// Sets the ``selectedSchool`` and zoom's into its coordinates.
    /// if the ``selectedSchool`` is nil, the ``mapRegion`` is set back to ``SchoolsMapView.defaultRegion``
    func highlight(school: SchoolObject?) {
        selectedSchool = school
        
        if let selectedSchool = selectedSchool,
           let coordinate = selectedSchool.coordinate {
            let newCoordinate = CLLocationCoordinate2D(latitude: coordinate.latitude-0.01,
                                                       longitude: coordinate.longitude)
            mapRegion = MKCoordinateRegion(
                    center: newCoordinate,
                    latitudinalMeters: 5000,
                    longitudinalMeters: 5000
                )
        } else {
            mapRegion = SchoolsMapView.defaultRegion
        }
    }
}
