//
//  _0240315_VitoRoyeca_NYCSchoolsApp.swift
//  20240315-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 3/15/24.
//

import SwiftUI
import SwiftData

@main
struct _0240315_VitoRoyeca_NYCSchoolsApp: App {
    /// schoolsViewModel is used SchoolsMapview and SchoolsListView to tack all school objects
    @StateObject private var schoolsViewModel = SchoolsViewModel()
    
    /// satResultsviewmodel is used in SchoolDetailsView
    @StateObject private var satResultsViewModel = SATResultsViewModel()

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(schoolsViewModel)    // we add the StateObjects to the environment so they can be used
                .environmentObject(satResultsViewModel) // in many places
        }
    }
}
