//
//  APIManagerTests.swift
//  20240315-VitoRoyeca-NYCSchoolsTests
//
//  Created by Vito Royeca on 3/18/24.
//

import XCTest

final class APIManagerTests: XCTestCase {

    /*
        Test the Schools API
     */
    func testFetchSchools() async throws {
        do {
            let url = URL(string: APIManager.shared.schoolsEndpoint)
            XCTAssert(url != nil)

            let (data,response) = try await URLSession.shared.data(from: url!)
            
            let httpResponse = response as? HTTPURLResponse
            XCTAssert(httpResponse != nil)
            XCTAssert(httpResponse!.statusCode == 200)
            
            let results = try JSONDecoder().decode([SchoolModel].self,
                                                  from: data)
            XCTAssert(!results.isEmpty)
            
        } catch {
            print(error)
            XCTFail("testFetchSchools() error")
        }
    }
    
    /*
        Test the SAT Result API. Note that the `dbn` parameter fetches only 1 record
     */
    func testFetchSATResult() async throws {
        do {
            let url = URL(string: "\(APIManager.shared.satResultsEndpoint)?dbn=01M292")
            XCTAssert(url != nil)

            let (data,response) = try await URLSession.shared.data(from: url!)
            
            let httpResponse = response as? HTTPURLResponse
            XCTAssert(httpResponse != nil)
            XCTAssert(httpResponse!.statusCode == 200)
            
            let results = try JSONDecoder().decode([SATResultModel].self,
                                                  from: data)
            XCTAssert(!results.isEmpty)
            
        } catch {
            print(error)
            XCTFail("testFetchSchools() error")
        }
    }

}
