# NYC High Schools

This is an iOS app the shows New York City's high schools.

# Screenshots

<img src="screenshots/01.png" width="200">
<img src="screenshots/02.png" width="200">
<img src="screenshots/03.png" width="200">

# Author

[Vito Royeca](mailto:vito.royeca@gmail.com)